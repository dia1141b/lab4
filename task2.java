/*Создать метод, который будет выводить указанный массив на экран в строку.
 *С помощью созданного метода и метода из предыдущей задачи заполнить
 *5 массивов из 10 элементов каждый случайными числами
 *и вывести все 5 массивов на экран, каждый на отдельной строке.
 */

/**
 *
 * @author dia1141b
 */
import java.util.Scanner;


public class task2 {
    //случайное число из интервала от а до б
   
    
    //ввод данных в масив
    public static void vvod(int[] ar) {
        int a = 0;
        int b = 0;
        Scanner sc = new Scanner(System.in);
        //проверка исключений
        try {
            System.out.println("Введите a");
            a = sc.nextInt();
            System.out.println("Введите b");
            b = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        //инициализация
        for (int i = 0; i < 10; i++) {
            ar[i] = task1.rand(a, b);
            System.out.print(ar[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] nums;
        nums = new int[10];
        for (int i = 1; i < 5; i++) {
            vvod(nums);
        }
    }
}
