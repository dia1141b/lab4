/*
 * 
 */

/**
 *
 * @author dia1141b
 */
import java.util.Scanner;

public class Task4 {

    public static int fib(int a) {
        if (a > 2) {
            return (fib(a - 2) + fib(a - 1));
        }
        return 1;
        
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        //проверка исключений
        try {
            System.out.println("Введите a");
            a = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        long timeMillis = System.currentTimeMillis();
        long t = 0;
        do {
            long c;
            a++;
            c = fib(a);
            t = System.currentTimeMillis() - timeMillis;
        } while (t <= 10000);
        System.out.println(a);
    }
}
