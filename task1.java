/*
 *Создать статический метод, который будет иметь
 *два целочисленных параметра a и b, и в качестве своего значения
 *возвращать случайное целое число из отрезка [a;b].
 *C помощью данного метода заполнить массив из 20 целых чисел и вывести его на экран.
 */

/**
 *
 * @author dia1141b
 */
 
import java.util.Scanner;

public class task1 {
         //случайноре число из интервал от а до б
    public static int rand(int a, int b) {
       
        if (a > b) {
            return (int) (Math.random() * (a-b+1) + b);
        } else {
            return (int) (Math.random() * (b-a+1) + a);
        }

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] nums;
        nums = new int[20];
        int a = 0;
        int b = 0;
         //проверка исключений
        try {
            System.out.println("Введите a");
            a = sc.nextInt();
            System.out.println("Введите b");
            b = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        //инициализайия и вывод
        for (int i = 0; i < 20; i++) {
            nums[i] = rand(a, b);
System.out.print(nums[i]+" ");
        }
    }
}

