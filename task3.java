/*
 *Создать метод, который будет сортировать указанный массив
 *по возрастанию любым известным вам способом.
 */

/**
 *
 * @author dia1141b
 */
public class task3 {

    //сортировка методом пузырька
    public static void sort(int[] ar) {

        for (int i = ar.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (ar[j] > ar[j + 1]) {
                    int c = ar[j];
                    ar[j] = ar[j + 1];
                    ar[j + 1] = c;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] nums;
        nums = new int[20];
        //инициализация и вывод
        for (int i = 0; i < 20; i++) {
            nums[i] = (int) (Math.random() * 89 + 10);
            System.out.print(nums[i] + " ");
        }
        System.out.println();
        
        //вызов метода
        sort(nums);
        
        //вывод отсортированого массива
        for (int i = 0; i < 20; i++) {
            System.out.print(nums[i] + " ");
        }
    }
}
